** What are views for? **

Views are the 'screens' of our project. We try to _rarely_ used any code in here, as it's a contrasting idea to our *small components* thinking.

The files in here would be named as the slug of the template (or similar) - for example `_single-team.scss`.

This *never* means that just because a block only exists in one view, that you plonk it in this folder. We're more thinking of some `:root`-level classes that change that view.

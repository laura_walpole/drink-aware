import PropTypes from 'prop-types';
import React from 'react';

const Button = props => (
    <button className={`btn btn--${props.type}`}>{props.children}</button>
);

Button.propTypes = {
    children: PropTypes.node.isRequired,
    type: PropTypes.string.isRequired
};

export default Button;

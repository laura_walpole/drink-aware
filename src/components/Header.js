// import PropTypes from 'prop-types';
import React from 'react';

// const Header = ({ children }) => (
const Header = () => (
    <h1 className="header pad">DrinkCompare Calculator</h1>
);

// Header.propTypes = {
//     children: PropTypes.node.isRequired
// };

export default Header;

import PropTypes from 'prop-types';
import React from 'react';

const Text = ({ children, Component, modifier }) => (
    <Component className={modifier}>{children}</Component>
);

Text.propTypes = {
    children: PropTypes.node.isRequired,
    Component: PropTypes.node.isRequired,
    modifier: PropTypes.string.isRequired
};

export default Text;

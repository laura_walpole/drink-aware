import React from 'react';

import Button from '../components/Button';
import Header from '../components/Header';
import Text from '../components/Text';

const IntroScreen = () => (
    <div>
        <Text Component="h2" modifier="text--something">How does your drinking compare to rest of the UK?</Text>
        
        <Text Component="p" modifier="text--something">Compare your drinking to the UK average.</Text>

        <Button type="button">Start</Button>
    </div>
);

export default IntroScreen;

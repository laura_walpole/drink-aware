import React from 'react';

// Screens
import IntroScreen from '../screens/IntroScreen';

//Components
import Header from '../components/Header';

const App = () => (
    <div>
    <Header />
        <IntroScreen />
    </div>
);

export default App;
